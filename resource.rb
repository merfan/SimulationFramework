class Resource

  def initialize(name, capacity=1)
    @name = name
    @capacity = capacity
  end

  attr_accessor :name, :capacity
end