require_relative 'future_event_list'

class Engine
  @@is_finished = false
  @@time = 0
  @@fel = FutureEventList.new


  def initialize(finish_condition)
    # finish_condition is a lambda expression that must return true on simulation finish conditions satisfied
    @finish_condition = finish_condition
    @@is_finished = false
    @@time = 0
    @@fel = FutureEventList.new
  end

  def simulate
    until @@is_finished
      next_event = @@fel.get_next_event
      @@time = next_event.time
      next_event.execute
      @@is_finished = @finish_condition.call
    end
  end

  def set_initial_event(event)
    @@fel.push_event(event)
  end

  def stat_repo=(repo)
    @repo = repo
  end

  def self.time
    @@time
  end

  def self.fel
    @@fel
  end

  attr_accessor :finish_condition, :repo
end