require 'pqueue'


class FutureEventList
  def initialize
    @queue = PQueue.new{ |a,b| a.time < b.time }
  end

  def get_next_event
    @queue.pop
  end

  def push_event(e)
    @queue.push(e)
  end

  alias :<< :push_event
end