class Process
  def initialize(action, queue_capacity=0)
    unless action.instance_of?(Action)
      raise 'Invalid Parameter Exception'
    end

    @queue = Queue.new
    @is_busy = false
    @queue_capacity = queue_capacity
    @delay = delay
    @action = action(delay)
  end


end