class Action
  def act
    raise 'No implementation found'
  end
end