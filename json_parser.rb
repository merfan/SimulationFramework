require 'json'
require_relative 'engine'
require_relative 'queueing_system/service_center'
require_relative 'queueing_system/server'
require_relative 'queueing_system/events/arrival_event'
require_relative 'queueing_system/events/departure_event'
require_relative 'queueing_system/events/delegation_event'
require_relative 'queueing_system/steps/arrival_step'
require_relative 'queueing_system/steps/departure_step'
require_relative 'queueing_system/steps/delegate_step'
require_relative 'queueing_system/steps/generate_arrival_event_step'
require_relative 'pipe_steps/statistics_step'
require_relative 'pipe_steps/create_entity_step'
require_relative 'queueing_system/statistics_repository'
require_relative 'pipe_steps/two_way_decide_step'



class JsonParser
  def initialize
    @app = {}
  end

  def get_app
    @app
  end

  def parse(input)
    input_json = JSON.parse(input)
    app_json = input_json['app']
    engine_json = app_json['engine']
    if engine_json.nil?
      raise ScriptError
    end


    finish_cond_json = engine_json['finish_cond']
    if finish_cond_json.nil?
      raise ScriptError
    end

    finish_cond = nil
    if finish_cond_json['type'] == 'time'
      max_time = finish_cond_json['value']
      finish_cond = lambda { Engine.time > max_time ? true : false }
    else
      raise ScriptError
    end

    engine = Engine.new(finish_cond)
    @app['engine'] = engine

    system_json = engine_json['system']

    if system_json['type'] == 'queue'
      parse_queue_system(system_json)
    else
      raise ScriptError
    end
    @app
  end

  private

  def parse_queue_system(system_json)
    config_json = system_json['config']
    service_centers_json= config_json['service_centers']
    service_centers = []
    @app['service_centers'] = {}
    service_centers_json.each do |sc|
      service_center = ServiceCenter.new(sc['name'])
      servers = []
      sc['servers'].each do |srv|
        name = srv['name']
        capacity = srv['capacity']
        distribution_json = srv['service_time_distribution']
        distribution = Distribution.new(distribution_json['type'], distribution_json['attrs'])
        server = Server.new(name, capacity, distribution)
        servers << server
      end
      service_center.servers = servers
      unless sc['server_selection_strategy'].nil?
        if sc['server_selection_strategy']['type'] == 'lambda'
          str = "Proc.new" + sc['server_selection_strategy']['condition']
          proc = eval(str)
          service_center.set_selection_strategy(proc)
        end
      end
      service_centers << service_center
      @app['service_centers'][sc['name']] = service_center
    end

    events_json = config_json['events']

    unless events_json['arrival'].nil?
      arrival_event_json = events_json['arrival']
      pipeline = []
      arrival_event_json.each do |step|
        case step['type']
          when 'create_entity'
            create = CreateEntityStep.new
            pipeline << create
          when 'arrival'
            attrs = step['attrs']
            service = @app['service_centers'][attrs['service_center']]
            arrival = ArrivalStep.new(service, attrs['output_event'])
            pipeline << arrival
          when 'generate_arrival_event'
            attrs = step['attrs']
            dist_json = attrs['interarrival_distribution']
            distro = Distribution.new(dist_json['type'], dist_json['attrs'])
            gen_arrival = GenerateArrivalEventStep.new(distro)
            pipeline << gen_arrival
          when 'decide'
            attrs = step['attrs']
            proc = nil
            if attrs['condition']['type'] == 'lambda'
              str = "Proc.new" + attrs['condition']['expression']
              proc = eval(str)
            end
            true_step_json = attrs['true_step']
            true_step = nil
            case true_step_json['type']
              when 'delegate'
                attrs = true_step_json['attrs']
                if !attrs['in_service_center'].nil?
                  in_service = @app['service_centers'][attrs['in_service_center']]
                else
                  in_service = nil
                end
                out_service = @app['service_centers'][attrs['out_service_center']]
                delegate = DelegateStep.new(in_service, out_service, attrs['output_event'])
                true_step = delegate
            end
            attrs = step['attrs']
            false_step_json = attrs['false_step']
            false_step = nil
            case false_step_json['type']
              when 'delegate'
                attrs = false_step_json['attrs']
                if !attrs['in_service_center'].nil?
                  in_service = @app['service_centers'][attrs['in_service_center']]
                else
                  in_service = nil
                end
                out_service = @app['service_centers'][attrs['out_service_center']]
                delegate = DelegateStep.new(in_service, out_service, attrs['output_event'])
                false_step = delegate
            end

            decide = TwoWayDecideStep.new(proc,true_step,false_step)
            pipeline << decide
        end
      end
      ArrivalEvent.pipeline = pipeline
    end

    unless events_json['departure'].nil?
      departure_event_json = events_json['departure']
      pipeline = []
      departure_event_json.each do |step|
        case step['type']
          when 'departure'
            attrs = step['attrs']
            service = @app['service_centers'][attrs['service_center']]
            departure = DepartureStep.new(service, attrs['output_event'])
            pipeline << departure
          when 'statistics'
            attrs = step['attrs']
            service_centers = []
            attrs['service_centers'].each do |sc|
              service_center = @app['service_centers'][sc['name']]
              service_centers << service_center
            end
            stat_repo = StatisticsRepository.new(service_centers)
            @app['engine'].stat_repo = stat_repo
            stat = StatisticsStep.new(stat_repo)
            pipeline << stat
        end
      end
      DepartureEvent.pipeline = pipeline
    end

    unless events_json['delegate'].nil?
      delegate_event_json = events_json['delegate']
      delegate_event_json.each do |step|
        case step['type']
          when 'delegate'
            pipeline = []
            attrs = step['attrs']
            in_service = @app['service_centers'][attrs['in_service_center']]
            out_service = @app['service_centers'][attrs['out_service_center']]
            delegate = DelegateStep.new(in_service, out_service, attrs['output_event'])
            pipeline << delegate
            DelegationEvent.set_pipeline(in_service, pipeline)
        end
      end
    end

    first_event_json = config_json['first_event']
    unless first_event_json.nil?
      case first_event_json['type']
        when 'arrival'
          event = ArrivalEvent.new(first_event_json['time'])
          @app['engine'].set_initial_event(event)
      end
    end

  end
end