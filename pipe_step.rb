class PipeStep
  def execute(entity)
    raise NotImplementedError, 'Implement this method'
  end
end