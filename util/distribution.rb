require 'croupier'
require_relative 'constant_distribution'
require_relative 'discrete_distribution'

class Distribution
  def initialize(type, options={})
    @distro = nil
    case type
      when 'exponential'
        # options={lambda:value}
        @distro = Croupier::Distributions::Exponential.new({lambda:options['lambda']})
      when 'uniform'
        # options={included:value, excluded:value}
        @distro = Croupier::Distributions::Uniform.new({included:options['included'], excluded:options['excluded']})
      when 'normal'
        # options={mean:value, std:value}
        @distro = Croupier::Distributions::Normal.new({mean:options['mean'], std:options['std']})
      when 'constant'
        # options={number:value}
        @distro = ConstantDistribution.new(options[:number])
      when 'discrete'
        # options={range1:value1, range2:value2, ...}
        @distro = DiscreteDistribution.new(options)
      else
        raise 'no such distribution found'
    end
  end

  def get_random
    rnd = @distro.generate_number
    while rnd < 0
      rnd = @distro.generate_number
    end
    rnd
  end

end