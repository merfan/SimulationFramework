require File.expand_path '../game_of_life', __FILE__
require 'chunky_png'
require 'mini_magick'
require "rubygems"
require "rmagick"
include Magick


def play(iterations)
  game = Game.new(40, 20)
  # oscillators:
  # game.load [2,20], [2,21], [2,22]
  # game.load [2,10], [2,11], [2,12], [3,9], [3,10], [3,11]
  # blinker:
  game.load [6,6],[6,7],[6,8]
  n = 0
  
  while n <= iterations
    png = ChunkyPNG::Image.new(100, 100, ChunkyPNG::Color::WHITE)
    grid = game.grid
    grid.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        unless cell.zero?
          png = png.rect(x*10, y*10, x*10+10, y*10+10, 'black', 'black')
          #png[y,x] = ChunkyPNG::Color('black')
        end
      end
    end
    png.save(n.to_s + '.png', :interlace => true)
    
    #print "\e[2J\e[f"
    #puts game.to_s.gsub('0', ' ').gsub('1', '@')
    game.tick
    #sleep 0.1
    n+=1
  end
  
  frames = n
   gif = ImageList.new

  j=0
  
  frames.times do |i|
    a = j.to_s+'.png'
    puts a
    File.open(a, "rb") do |f|
	  img = Magick::Image::from_blob(f.read).first
      gif << img
    end  
    j = j + 1
 end

  gif.write("rub_test.gif")
end

play(8)
