require 'time'

class DiscreteDistribution
  @@r = Random.new(Random.new_seed)

  def initialize(distribution_hash)
    @table = distribution_hash
  end

  def generate_number
    rand = @@r.rand(100) + 1

    @table.each do |key, value|
      if key.include?(rand)
        return value
      end
    end
  end
end