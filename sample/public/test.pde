INIT_DATA = [[15,1],[15,2],[16,1],[16,2],[13,13],[13,14],[14,12],[14,16],[15,11],[15,17],[16,11],[16,15],[16,17],[16,18],[17,11],[17,17],[18,12],[18,16],[19,13],[19,14],[11,25],[12,23],[12,25],[13,21],[13,22],[14,21],[14,22],[15,21],[15,22],[16,23],[16,25],[17,25],[13,35],[13,36],[14,35],[14,36]];
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Globals
int STATE = 0;
int CA_WIDTH = 800;	
int CA_HEIGHT = 600;
int SPEED = 3;	//speed and pixel size
int PWIDTH = ceil(CA_WIDTH/SPEED);
int PHEIGHT = ceil(CA_HEIGHT/SPEED);
color PIXEL_COLOR[] = {color(50), color(200,240,180)};

int GRID_A[][];
int GRID_B[][];

int RULE_SURVIVE[] = {2,3};
int RULE_BIRTH[] = {3};
int RULE_GENS = 2;


//Setup the canvas.
void setup() {
	size(CA_WIDTH, CA_HEIGHT);
	noStroke();
	frameRate(30);

	//Init the grid
	GRID_A = new int[PWIDTH][PHEIGHT];
	GRID_B = new int[PWIDTH][PHEIGHT];

	for (int j=0; j<INIT_DATA.length; j++){
	    GRID_B[INIT_DATA[j][1]][INIT_DATA[j][0]] = 1;
	}
	for(int x=0; x < PWIDTH; x++) {
        for(int y=0; y < PHEIGHT; y++) {
            if (GRID_B[x][y] > 1) {
                GRID_B[x][y] = 0;
            }
        }
    }
    noLoop();
}

//Main draw loop. Is called continuously.
void draw() {
	background(PIXEL_COLOR[0]);

	for(int x=0; x < PWIDTH; x++) {
	   for(int y=0; y < PHEIGHT; y++) {
		int currentState = GRID_B[x][y];
		if ( currentState > 0) {
		     drawPoint(x,y, PIXEL_COLOR[currentState]);
		     GRID_A[x][y] = currentState;
		}
		else if (currentState <= 0) {
		     GRID_A[x][y] = currentState;
		}
		GRID_B[x][y] = 0;
	   }
	}

	//This loop calculates the next generation of cells.
	//Cellular Automata rules should be specified here.
	for(int x=0; x < PWIDTH; x++) {
            for(int y=0; y < PHEIGHT; y++) {
		int currentState = GRID_A[x][y];

		if ( currentState == 0) {
			int neighborsOn = calcNeighbors(x,y);
			if ( ruleContains(neighborsOn, RULE_BIRTH))
				GRID_B[x][y] = 1;
		}
		else if ( currentState > 0 && (currentState < (RULE_GENS - 1) || RULE_GENS == 2) ) {
			
			int neighborsOn = (RULE_SURVIVE.length == 0) ? 0 : calcNeighbors(x,y);
			bool shouldSurvive = ruleContains(neighborsOn, RULE_SURVIVE);
			if (currentState == 1 && shouldSurvive) 
			{
				GRID_B[x][y] = currentState;
			}
			else if (!shouldSurvive) {
				    GRID_B[x][y] = (currentState + 1) % RULE_GENS;
			}

			if ( currentState > 1)
				GRID_B[x][y] = currentState + 1;
		}
		else if (currentState >= (RULE_GENS - 1)) {
			GRID_B[x][y] = 0;
		}
		



	    }
	}
	
}

//Given a cell, how many neighbors are in the "on" state?
int calcNeighbors(int x,int y) {
	int totalSum = 0;
	int xLeft = (x + PWIDTH - 1) % PWIDTH;
	int xRight = (x + 1) % PWIDTH;
	int yTop = (y + PHEIGHT -1) % PHEIGHT;
	int yBottom = (y + 1) % PHEIGHT;
	
	if (GRID_A[xLeft][yTop] == 1) totalSum++;
	if (GRID_A[x][yTop] ==1 ) totalSum++;
	if (GRID_A[xRight][yTop] ==1 ) totalSum++;
	if (GRID_A[xLeft][y] ==1 ) totalSum++;
	if (GRID_A[xRight][y] ==1 ) totalSum++;
	if (GRID_A[xLeft][yBottom] ==1 ) totalSum++;
	if (GRID_A[x][yBottom] ==1 ) totalSum++;
	if (GRID_A[xRight][yBottom] ==1 ) totalSum++;

	return totalSum;
}

void clearGRID() {
	for(int x=0; x < PWIDTH; x++) {
		for(int y=0; y < PHEIGHT; y++) {
			GRID_B[x][y] = 0;
		}
	}
}

void drawPoint(int x, int y, color c) {
	fill(c);
	rect(x*SPEED,y*SPEED,SPEED,SPEED);
}

bool ruleContains(int n, Array rule) {
	for(int i=0; i < rule.length; i++) {
		if ( rule[i] == n )
			return true;
	}
	return false;
}

//Called when user drags mouse over canvas. This will let you draw pixels.
void mouseClicked() {
	int xPos = ceil(mouseX / SPEED);
	int yPos = ceil(mouseY / SPEED);
	int xPosPrev = ceil(pmouseX / SPEED);
	int yPosPrev = ceil(pmouseY / SPEED);

	int pixelType = (mouseButton == LEFT) ? 1 : 0;
	GRID_B[xPos][yPos] = pixelType;
    drawPoint(xPos,yPos, PIXEL_COLOR[pixelType]);
}

void keyPressed()
{
    if(STATE == 0){
     	loop();
     	STATE = 1;
    }
    else{
        noLoop();
        STATE = 0;
    }
}