require 'sinatra'
require 'json'
require '../util/gol/game_of_life'
require 'chunky_png'
require 'mini_magick'
require "rubygems"
require "rmagick"
include Magick
require_relative '../queueing_system/events/arrival_event'
require_relative '../queueing_system/customer'
require_relative '../queueing_system/queueing_system'
require_relative '../json_parser'
require_relative '../engine'
require_relative '../inventory_system/order_event'
require_relative '../inventory_system/steps/order_step'
require_relative '../inventory_system/steps/generate_next_order_step'
require_relative '../inventory_system/steps/demand_step'
require_relative '../inventory_system/steps/generate_next_demand_step'
require_relative '../inventory_system/entities/stock_item'
require_relative '../inventory_system/stock'
require_relative '../util/distribution'
require_relative '../inventory_system/steps/statistic_step'
require_relative '../inventory_system/inventory_statistics_repository'
require_relative '../queueing_system/statistics_repository'

get '/' do
  erb :server
end

post '/run' do
  post = params[:post]
  code = post[':code']
  parser = JsonParser.new
  parser.parse(code)
  app = parser.get_app
  app['engine'].simulate
  statistics = app['engine'].repo
  statistics.print_results
  @printable_result = statistics.results
  ## chart1
  average_delay = {}
  statistics.delay_times.each do |s|
    average_delay.has_key?(s.round.to_s) ? average_delay[s.round.to_s] +=1 : average_delay.store(s.round.to_s, 1)
  end
  @chart1 = average_delay.to_json

  ## chart2
  time_in_system = {}
  time_in_system2 = {}
  statistics.time_in_system_list.each do |s|
    time_in_system.has_key?(s.round.to_s) ? time_in_system[s.round.to_s] +=1 : time_in_system.store(s.round.to_s, 1)
    time_in_system2.has_key?((s/10).round.to_s) ? time_in_system2[(s/10).round.to_s] +=1 : time_in_system2.store((s/10).round.to_s, 1)
  end
  @chart2 = time_in_system2.to_json
  # p @chart2.to_s + ' is chart 2'
  erb :results

end

get '/inventory' do
  finish_cond = lambda { Engine.time > 20 ? true : false }
  engine = Engine.new(finish_cond)

##
# initializing Inventory system simulation
  stock = Stock.new('dakke',500)
  stat_repository = InventoryStatisticsRepository.new({})
  engine.stat_repo = stat_repository
  InventorySystem.stocks['dakke'] = stock
  item = StockItem.new('newspaper',0,33,50,1,0.1)
  order_distribution = Distribution.new('constant', {number:70})
  order_step = OrderStep.new('dakke', order_distribution,'newspaper',33,50,1,0.1)
  order_generator = GenerateNextOrderStep.new(Distribution.new('constant', {number:1}))
  demand_generator = GenerateNextDemandStep.new(Distribution.new('constant', {number:1.5}))
  OrderEvent.pipeline = [order_step, order_generator, demand_generator]
  range1 = 1..25
  range2 = 26..70
  range3 = 71..85
  range4 = 86..100
  demand_step = DemandStep.new('dakke',item, Distribution.new('discrete',{range1=>10, range2=>25, range3=>75, range4=>85}))
  stat = StatisticStep.new(stat_repository)
  DemandEvent.pipeline =[demand_step,stat]

  first_order = OrderEvent.new(0)

##
# start simulation
  engine.set_initial_event(first_order)
  engine.simulate
  @final_profit = StockItem.profit
## chart1
  daily_chart_data = {}
  stat_repository.daily_profit.each do |dp, v|
    daily_chart_data.has_key?(v.round.to_s) ? daily_chart_data[v.round.to_s] +=1 : daily_chart_data.store(v.round.to_s, 1)
  end
  @chart1 = daily_chart_data.to_json

## chart2
  @chart2 = stat_repository.daily_profit.to_json
  erb :inventory_results
end

##################################### GOL
def play(iterations, input = [])
  game = Game.new(500, 500)
  if input != []
    game.load(input)
    # game.load [[2,10], [2,11], [2,12], [3,9], [3,10], [3,11]]

  else
    # oscillators:
    # game.load [[2,20], [2,21], [2,22]]
    gun = [[15,1],[15,2],[16,1],[16,2],[13,13],[13,14],[14,12],[14,16],[15,11],[15,17],[16,11],[16,15],[16,17],[16,18],[17,11],[17,17],[18,12],[18,16],[19,13],[19,14],[11,25],[12,23],[12,25],[13,21],[13,22],[14,21],[14,22],[15,21],[15,22],[16,23],[16,25],[17,25],[13,35],[13,36],[14,35],[14,36]]
    game.load(gun)
    # game.load [[2,10], [2,11], [2,12], [3,9], [3,10], [3,11]]
    # blinker:
    #game.load [[6,6],[6,7],[6,8],[6,3]]
  end

  n = 0

  while n <= iterations
    png = ChunkyPNG::Image.new(500, 500, ChunkyPNG::Color::WHITE)
    grid = game.grid
    grid.each_with_index do |row, y|
      row.each_with_index do |cell, x|
        unless cell.zero?
          png = png.rect(x*5, y*5, x*5+5, y*5+5, 'black', 'black')
          #png[y,x] = ChunkyPNG::Color('black')
        end
      end
    end
    png.save('public/gol/' + n.to_s + '.png', :interlace => true)

    #print "\e[2J\e[f"
    #puts game.to_s.gsub('0', ' ').gsub('1', '@')
    game.tick
    #sleep 0.1
    n+=1
  end

  frames = n
  gif = ImageList.new

  j=0

  frames.times do |i|
    a = 'public/gol/' + j.to_s+'.png'
    puts a
    File.open(a, "rb") do |f|
      img = Magick::Image::from_blob(f.read).first
      gif << img
    end
    j = j + 1
  end

  gif.write("public/gol/rub_test.gif")
end

get '/gol' do
  play(8)
  erb :gol
end

post '/gol' do
  p_it = params['iteration']
  p_in = '[' + params['input'] + ']'
  # puts p_in.to_s + " input"
  # puts p_it.to_s + " iteration"
  # puts p_in.class
  play(p_it.to_i, eval(p_in) )
  erb :gol
end

get '/gol_input' do
  erb :gol_input
end
get '/gol2' do
  erb :gol2
end