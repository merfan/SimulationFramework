require_relative '../../engine'
require_relative '../../inventory_system/order_event'
require_relative '../../inventory_system/steps/order_step'
require_relative '../../inventory_system/steps/generate_next_order_step'
require_relative '../../inventory_system/steps/demand_step'
require_relative '../../inventory_system/steps/generate_next_demand_step'
require_relative '../../inventory_system/entities/stock_item'
require_relative '../../inventory_system/stock'
require_relative '../../util/distribution'
require_relative '../../inventory_system/steps/statistic_step'



##
# defining finish condition and main simulation engine
finish_cond = lambda { Engine.time > 20 ? true : false }
engine = Engine.new(finish_cond)

##
# initializing Inventory system simulation
stock = Stock.new('dakke',500)
stat_repository = InventoryStatisticsRepository.new({})
engine.stat_repo = stat_repository
InventorySystem.stocks['dakke'] = stock
item = StockItem.new('newspaper',0,33,50,1,0.1)
order_distribution = Distribution.new('constant', {number:70})
order_step = OrderStep.new('dakke', order_distribution,'newspaper',33,50,1,0.1)
order_generator = GenerateNextOrderStep.new(Distribution.new('constant', {number:1}))
demand_generator = GenerateNextDemandStep.new(Distribution.new('constant', {number:1.5}))
OrderEvent.pipeline = [order_step, order_generator, demand_generator]
range1 = 1..25
range2 = 26..70
range3 = 71..85
range4 = 86..100
demand_step = DemandStep.new('dakke',item, Distribution.new('discrete',{range1=>10, range2=>25, range3=>75, range4=>85}))
stat = StatisticStep.new(stat_repository)
DemandEvent.pipeline =[demand_step,stat]

first_order = OrderEvent.new(0)

##
# start simulation
engine.set_initial_event(first_order)
engine.simulate