require '../engine'
require '../queueing_system/events/arrival_event'
require '../queueing_system/customer'
require_relative '../queueing_system/queueing_system'
require_relative '../queueing_system/steps/delegate_step'
require_relative '../pipe_steps/assign_step'
require_relative '../statistics_repository'

##
# defining finish condition and main simulation engine
finish_cond = lambda { Engine.time > 200 ? true : false }
engine = Engine.new(finish_cond)

##
# initializing Queuing system simulation for call center problem

simple_service_center = ServiceCenter.new('simple service center')
server1 = Server.new('simple', 1, Distribution.new('normal', {mean: 2, std: 0.5}))
simple_service_center.servers << server1

QueueingSystem.service_centers = [simple_service_center]

##
# initialize arrivals
customer_attributes = {:test1 => 0}
create = CreateEntityStep.new(customer_attributes)
arrival = ArrivalStep.new(simple_service_center,'delegate')
next_arrival_generator = GenerateArrivalEventStep.new(Distribution.new('normal', {mean: 8, std: 5}))
arrival_event_pipeline = [create, arrival, next_arrival_generator]
ArrivalEvent.pipeline = arrival_event_pipeline


first_arrival = ArrivalEvent.new(0)



##
# initializing Queuing system simulation for call center problem
free_cmp = Proc.new { |a, b|
  if (a.name == 'able' && !a.busy?)
    1
  elsif (b.name == 'able' && !b.busy?)
    -1
  elsif (!a.busy?)
    1
  elsif (!b.busy?)
    -1
  elsif (a.becomes_idle < b.becomes_idle)
    1
  elsif (b.becomes_idle < a.becomes_idle)
    -1
  end
}

callcenter_service_center = ServiceCenter.new('callcenter service center', free_cmp)
server1 = Server.new('able', 1, Distribution.new('normal', {mean: 50, std: 5}))
server2 = Server.new('baker', 1, Distribution.new('normal', {mean: 3.5, std: 1}))
callcenter_service_center.servers << server1
callcenter_service_center.servers << server2
QueueingSystem.service_centers << callcenter_service_center

##
# initialize arrivals
# create = CreateEntityStep.new('create')

# arrival = ArrivalStep.new(service, 'departure')
# next_arrival_generator = GenerateArrivalEventStep.new('arrival', Distribution.new('normal', {mean: 2.5, std: 5}))
# statistics = StatisticsStep.new(QueueingSystem.service_centers)
# arrival_event_pipeline = [create, arrival, next_arrival_generator, statistics]
# ArrivalEvent.pipeline = arrival_event_pipeline

departure = DepartureStep.new(QueueingSystem.service_centers[1], 'departure')
stat_repo = InventoryStatisticsRepository.new(QueueingSystem.service_centers, customer_attributes)
statistics = StatisticsStep.new(stat_repo)
assign = AssignStep.new(Proc.new{ |a|
                          a.add_attribute(:test1, a.get_attribute(:test1) + 1)
                        a})
DepartureEvent.pipeline= [departure, statistics, assign]

# first_arrival = ArrivalEvent.new(0)

delegate = DelegateStep.new(QueueingSystem.service_centers[0],QueueingSystem.service_centers[1],'arrival','departure')
DelegationEvent.pipeline= [delegate]

##
# start simulation
engine.set_initial_event(first_arrival)
engine.simulate
stat_repo.print_results
stat_repo.print_user_defined_attr
