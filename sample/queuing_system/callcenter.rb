require '../engine'
require '../queueing_system/events/arrival_event'
require '../queueing_system/customer'
require_relative '../queueing_system/queueing_system'

##
# defining finish condition and main simulation engine
finish_cond = lambda { Engine.time > 500 ? true : false }
engine = Engine.new(finish_cond)

##
# initializing Queuing system simulation for call center problem
free_cmp = Proc.new { |a, b|
  if a.name == 'able' && !a.busy?
    1
  elsif b.name == 'able' && !b.busy?
    -1
  elsif !a.busy?
    1
  elsif !b.busy?
    -1
  elsif a.becomes_idle < b.becomes_idle
    1
  elsif b.becomes_idle < a.becomes_idle
    -1
  end
}
service_center = ServiceCenter.new('service center 1', free_cmp)
server1 = Server.new('able', 1, Distribution.new('normal', {mean: 8, std: 5}))
server2 = Server.new('baker', 1, Distribution.new('normal', {mean: 3.5, std: 1}))
service_center.servers << server1
service_center.servers << server2
QueueingSystem.service_centers = [service_center]

##
# initialize arrivals
customer_attributes = {:test1 => 0}
create = CreateEntityStep.new(customer_attributes)
service = QueueingSystem.service_centers[0]
arrival = ArrivalStep.new(service, 'departure')
next_arrival_generator = GenerateArrivalEventStep.new(Distribution.new('normal', {mean: 2.5, std: 5}))
statistics = StatisticsStep.new(QueueingSystem.service_centers, customer_attributes)
arrival_event_pipeline = [create, arrival, next_arrival_generator]
ArrivalEvent.pipeline = arrival_event_pipeline

departure = DepartureStep.new(QueueingSystem.service_centers[0], 'departure')
DepartureEvent.pipeline= [departure, statistics]

first_arrival = ArrivalEvent.new(0)

##
# start simulation
engine.set_initial_event(first_arrival)
engine.simulate
statistics.print_results
statistics.print_user_defined_attr
