require '../engine'
require '../queueing_system/events/arrival_event'
require '../queueing_system/customer'
require_relative '../queueing_system/queueing_system'

##
# defining finish condition and main simulation engine
finish_cond = lambda { Engine.time > 500 ? true : false }
engine = Engine.new(finish_cond)

##
# initializing Queuing system simulation

service_center = ServiceCenter.new('simple service center')
server1 = Server.new('able', 1, Distribution.new('normal', {mean: 8, std: 5}))
service_center.servers << server1

QueueingSystem.service_centers = [service_center]

##
# initialize arrivals
create = CreateEntityStep.new()
service = QueueingSystem.service_centers[0]
arrival = ArrivalStep.new(service, 'departure')
next_arrival_generator = GenerateArrivalEventStep.new(Distribution.new('exponential', {lambda: 2.5}))
arrival_event_pipeline = [create, arrival, next_arrival_generator]
ArrivalEvent.pipeline = arrival_event_pipeline

departure = DepartureStep.new(service_center, 'departure')
statistics = StatisticsStep.new(QueueingSystem.service_centers)
DepartureEvent.pipeline= [departure, statistics]

first_arrival = ArrivalEvent.new(0)

##
# start simulation
engine.set_initial_event(first_arrival)
engine.simulate
statistics.print_results
