class Entity

  def initialize(name)
    @name = name
    @service_time = 0
    @attributes = {}
  end

  def add_attribute(name, value)
    @attributes[name] = value
  end

  def get_attribute(name)
    @attributes[name]
  end

  attr_accessor :name, :service_time, :attributes
end