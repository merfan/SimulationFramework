require_relative 'pipeline'

class Event
  def initialize
    @time = 0
    @pipeline = Pipeline.new([])
  end

  def execute
    raise NotImplementedError, 'Implement this method'
  end

  attr_accessor :time
end