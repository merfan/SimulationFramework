class Pipeline
  def initialize(pipe_array)
    @pipe_array = pipe_array
  end

  def add_last(step)
    @pipe_array << step
  end

  def add_first(step)
    @pipe_array.insert(0,step)
  end

  def execute(entity)
    ent = entity
    @pipe_array.each do |step|
      ent = step.execute(ent)
    end
    ent
  end

end