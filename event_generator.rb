require_relative 'queueing_system/events/departure_event'
require_relative 'queueing_system/events/arrival_event'
require_relative 'queueing_system/events/delegation_event'

class EventGenerator
  def self.generate_event(type, time, entity, index=nil)
    case type
      when 'departure'
        return DepartureEvent.new(time, entity)
      when 'arrival'
        return ArrivalEvent.new(time)
      when 'delegate'
        return DelegationEvent.new(time, entity, index)
    end
  end
end