class InventoryStatisticsRepository
  def initialize(user_attrs={})
    @user_defined_attrs = user_attrs
    @attr_list = {}
    @daily_profit = {}
    @user_defined_attrs.each do |uda, val|
      @attr_list[uda] = Array.new
    end
    @results = ''
  end

  def print_results
    # @results += 'Total simulation time : ' + Engine.time.to_s + ' <br><br>'+"\n"
    # # @results += 'Service times of customers: ' + @service_times.to_s + ' <br><br>'+"\n"
    # @results += 'Average Service time: ' + (@service_times.inject { |sum, el| sum + el }.to_f / @service_times.size).to_s + ' <br><br>'+"\n"
    # # @results += 'Customer delay times: ' + @delay_times.to_s + ' <br><br>'+"\n"
    # @results += 'Average delay: ' + (@delay_times.inject { |sum, el| sum + el }.to_f / @delay_times.size).to_s + ' <br><br>'+"\n"
    # # @results += 'Customer time in system: ' + @time_in_system_list.to_s + ' <br><br>'+"\n"
    # @results += 'Average time in system: ' + (@time_in_system_list.inject { |sum, el| sum + el }.to_f / @time_in_system_list.size).to_s + ' <br><br>'+"\n"
    # calc_utilization
    # entity_attrs
    # puts @results
    # @results
  end

  def print_user_defined_attr
    p @attr_list.to_s
  end

  attr_accessor :user_defined_attrs, :attr_list, :daily_profit

end