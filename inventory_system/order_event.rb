require_relative '../event'

class OrderEvent < Event
  @@pipeline = nil

  def initialize(time)
    @time = time
  end

  def self.pipeline
    @@pipeline
  end

  def self.pipeline=(pipeline)
    @@pipeline = pipeline
  end

  def execute
    p = Pipeline.new(@@pipeline)
    p.execute(nil)
  end

  attr_accessor :time
end