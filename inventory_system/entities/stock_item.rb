require_relative '../../entity'


class StockItem < Entity
  @@profit = {}

  def initialize(name, amount, cost, price, expires, salvage_percent)
    @name = name
    @amount = amount
    @cost = cost
    @price = price
    @expires = expires
    @salvage_percent = salvage_percent
    @daily_profit = 0
  end

  def self.profit
    @@profit
  end

  def self.profit= (profit)
    @@profit = profit
  end

  attr_accessor :name, :amount, :cost, :price, :expires, :salvage_percent, :daily_profit
end