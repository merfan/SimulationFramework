class DemandEvent < Event
  @@pipeline = nil

  def initialize(time, entity)
    @time = time
    @entity = entity
  end

  def self.pipeline
    @@pipeline
  end

  def self.pipeline=(pipeline)
    @@pipeline = pipeline
  end

  def execute
    p = Pipeline.new(@@pipeline)
    p.execute(@entity)
  end

  attr_accessor :time
end