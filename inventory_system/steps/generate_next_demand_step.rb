require_relative '../demand_event'
require_relative '../../pipe_step'
require_relative '../../engine'

class GenerateNextDemandStep < PipeStep

  def initialize(demand_time_distribution)
    @distribution = demand_time_distribution
  end

  def execute(entity)
    time = @distribution.get_random
    event = DemandEvent.new(Engine.time + time, entity)
    Engine.fel << event
    entity
  end

end