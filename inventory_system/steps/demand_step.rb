require_relative '../../pipe_step'
require_relative '../../inventory_system/inventory_system'
require_relative '../../inventory_system/entities/stock_item'
require_relative '../../engine'


class DemandStep < PipeStep

  def initialize(stock, demand_item, amount_distributon)
    @demand_item = demand_item
    @distro = amount_distributon
    @stock = stock
  end

  def execute(entity)
    amount = @distro.get_random
    items = InventorySystem.stocks[@stock].get_items(@demand_item)
    remaining = amount

    items.each_pop do |item|
      if item.expires < Engine.time
        StockItem.profit[entity.name] += (item.price * item.salvage_percent) * item.amount
        entity.daily_profit += (item.price * item.salvage_percent) * item.amount
      else
        if item.amount < remaining
          StockItem.profit[entity.name] += item.price * item.amount
          entity.daily_profit += item.price * item.amount
          remaining -= item.amount
        else
          StockItem.profit[entity.name] += item.price * remaining
          entity.daily_profit += item.price * remaining
          item.amount -= remaining
          remaining = 0
          InventorySystem.stocks[@stock].add_item(item)
          break
        end
      end
    end

    unless remaining == 0
      StockItem.profit[entity.name] -= @demand_item.price * remaining
      entity.daily_profit -= @demand_item.price * remaining
    end
    entity
  end
end