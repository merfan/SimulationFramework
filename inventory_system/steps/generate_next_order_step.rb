require_relative '../order_event'
require_relative '../../pipe_step'
require_relative '../../engine'

class GenerateNextOrderStep < PipeStep

  def initialize(order_time_distribution)
    @distribution = order_time_distribution
  end

  def execute(entity)
    time = @distribution.get_random
    event = OrderEvent.new(Engine.time + time)
    Engine.fel << event
    entity
  end

end