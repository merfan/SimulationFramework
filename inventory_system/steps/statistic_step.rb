require_relative '../../engine'
require_relative '../../inventory_system/entities/stock_item'

class StatisticStep < PipeStep

  def initialize(repo)
    @repo = repo
  end

  def execute(entity)
    @repo.daily_profit[Engine.time] = entity.daily_profit
    p 'profit at ' + Engine.time.to_s + ' ' +  entity.daily_profit.to_s
    entity
  end

end