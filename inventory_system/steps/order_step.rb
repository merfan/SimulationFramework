require_relative '../../inventory_system/inventory_system'
require_relative '../../inventory_system/entities/stock_item'
require_relative '../../engine'
require_relative '../../pipe_step'


class OrderStep < PipeStep

  def initialize(stock, order_distribution, si_name, si_cost, si_price, si_expires, si_salvage_percent)
    @stock = stock
    @order_distribution = order_distribution
    @si_name = si_name
    @si_cost = si_cost
    @si_price = si_price
    @si_expires = si_expires
    @si_salvage_percent = si_salvage_percent
  end

  def execute(entity)
    order_amount = @order_distribution.get_random
    item = StockItem.new(@si_name, order_amount, @si_cost, @si_price, Engine.time + @si_expires, @si_salvage_percent)
    InventorySystem.stocks[@stock].add_item(item)
    if StockItem.profit[item.name].nil?
      StockItem.profit[item.name] = 0
    end
    StockItem.profit[item.name] -= item.amount * item.cost
    item.daily_profit -= item.amount * item.cost
    item
  end

end