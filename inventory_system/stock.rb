require 'pqueue'

class Stock
  def initialize(name, capacity)
    @name = name
    @capacity = capacity
    @size = 0
    @items = {}
  end

  def add_item(item)
    if @items[item.name].nil?
      @items[item.name] = PQueue.new{ |a,b| a.expires < b.expires }
    end

    @size += item.amount
    @items[item.name] << item
  end

  def get_items(item)
    @items[item.name]
  end

  attr_reader :capacity
  attr_accessor :size
end