class AssignStep < PipeStep
  def initialize(assign_proc)
    @assign_proc = assign_proc
  end

  def execute(customer)
    customer = @assign_proc.call(customer)
  end

end