require_relative '../pipe_step'
require_relative '../entity'

class TwoWayDecideStep < PipeStep
  def initialize(condition, true_step, false_step)
    # if !true_step.instance_of?(PipeStep) || !false_step.instance_of?(PipeStep) || !condition.instance_of?(Proc)
    #   raise ArgumentError('wrong type of argument passed to method')
    # end
    @condition = condition
    @true_step = true_step
    @false_step = false_step
  end

  def execute(entity)
    if @condition.call
      return @true_step.execute(entity)
    else
      return @false_step.execute(entity)
    end
  end
end