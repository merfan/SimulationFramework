require_relative '../pipe_step'
require_relative '../engine'

class StatisticsStep < PipeStep

  def initialize(repo)
    @repo = repo
  end

  def execute(entity)
    @repo.service_times << entity.service_time
    @repo.delay_times << entity.get_attribute(:delay)
    @repo.time_in_system_list << entity.get_attribute(:time_in_system)
    @repo.user_defined_attrs.each do |attr, val|
      a = entity.get_attribute(attr)
      unless @repo.attr_list[attr].nil?
        @repo.attr_list[attr] << a
      end
    end
    entity
  end

end