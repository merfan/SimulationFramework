require_relative '../pipe_step'
require_relative '../entity'
require_relative '../queueing_system/customer'

class CreateEntityStep < PipeStep
  def initialize(entity_attrs = {})
    @entity_attrs = entity_attrs
  end

  def execute(entity)
    # entity_class = Object.const_get(@entity_type)
    # entity = entity_class.new(@entity_attrs)
    Customer.new('customer', @entity_attrs)
  end
end