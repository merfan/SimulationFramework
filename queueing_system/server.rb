require 'colorize'
require_relative '../resource'

class Server < Resource

  def initialize(name, capacity, distribution)
    @name = name
    @becomes_idle = 0
    @seizing_entities = []
    @capacity = capacity
    @distribution = distribution
    @total_serv_time = 0
  end

  def seize(customer)
    if @seizing_entities.size < @capacity
      @seizing_entities << customer
      @total_serv_time += customer.service_time
      puts (@name.to_s + ' ' + 'seized a customer, id: ' + customer.__id__.to_s)
               .colorize(:color => :red, :background => :white)
    end
  end

  def release(customer)
    @seizing_entities.delete(customer)
    puts (@name.to_s + ' ' + 'released a customer, id: ' + customer.__id__.to_s)
             .colorize(:color => :blue, :background => :white)
  end

  def busy?
    if @seizing_entities.size == @capacity
      true
    else
      false
    end
  end

  attr_accessor :becomes_idle, :name, :distribution, :total_serv_time
end