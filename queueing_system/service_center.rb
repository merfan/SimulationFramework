require_relative '../engine'

class ServiceCenter
  def initialize(name, selection_strategy=nil)
    @name = name
    if selection_strategy != nil
      @selection_strategy = selection_strategy
    else
      @selection_strategy = Proc.new{
        |a,b| if a.becomes_idle < b.becomes_idle then 1 else -1 end
      }
    end
    @servers = []
    @queue = Queue.new
  end

  def set_selection_strategy(strategy)
    @selection_strategy = strategy
  end

  def get_free_server
    @servers = @servers.sort{ |a,b| @selection_strategy.call(a,b) }
    # @servers.each do |server|
    #   print server.name + " "
    # end
    # puts " "
    @servers.last
  end

  def get_depart_server
    # p 'Depart servers: ' +  @servers.to_s
    @servers.each do |server|
      if server.becomes_idle == Engine.time
        return server
      end
    end
  end

  attr_accessor :distribution, :queue, :name, :servers
end