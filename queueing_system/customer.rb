require_relative '../entity'

class Customer < Entity
  def initialize(name, attrs = {})
    super(name)
    @attributes = attrs
    add_attribute(:delay, 0)
    add_attribute(:arrival_time, 0)
    add_attribute(:time_in_system, 0)
  end

end