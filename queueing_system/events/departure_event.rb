require_relative '../../queueing_system/steps/departure_step'

class DepartureEvent < Event

  @@pipeline = []


  def initialize(time, entity)
    @time = time
    @entity = entity
  end

  def self.pipeline
    @@pipeline
  end

  def self.pipeline=(pipeline)
    @@pipeline = pipeline
  end

  def execute
    p = Pipeline.new(@@pipeline)
    p.execute(@entity)
  end

end