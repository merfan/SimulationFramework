class DelegationEvent < Event

  @@pipeline = {}

  def initialize(time, entity, index)
    @time = time
    @entity = entity
    @index = index
  end

  def self.pipeline
    @@pipeline[@index]
  end

  def self.set_pipeline(index, pipeline)
    @@pipeline[index] = pipeline
  end

  def execute
    p = Pipeline.new(@@pipeline[@index])
    p.execute(@entity)
  end

end