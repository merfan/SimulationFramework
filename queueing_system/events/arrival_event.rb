require_relative '../../event'
require_relative '../../pipe_steps/create_entity_step'
require_relative '../../queueing_system/service_center'
require_relative '../../queueing_system/steps/arrival_step'
require_relative '../../queueing_system/steps/generate_arrival_event_step'
require_relative '../../pipe_steps/statistics_step'
require_relative '../../util/distribution'
require_relative '../../queueing_system/queueing_system'

class ArrivalEvent < Event
  @@pipeline = nil

  def initialize(time)
    @time = time
  end

  def self.pipeline
    @@pipeline
  end

  def self.pipeline=(pipeline)
    @@pipeline = pipeline
  end

  def execute
    p = Pipeline.new(@@pipeline)
    p.execute(nil)
  end

  attr_accessor :time
end