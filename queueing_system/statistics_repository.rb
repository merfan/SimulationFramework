require 'colorize'

class StatisticsRepository
  def initialize(service_centers, user_attrs={})
    @service_centers = service_centers
    @service_times = []
    @interarrival_times = []
    @delay_times = []
    @time_in_system_list = []
    @user_defined_attrs = user_attrs
    @attr_list = {}
    @user_defined_attrs.each do |uda, val|
      @attr_list[uda] = Array.new
    end

    @results = ""
  end

  def print_results
    @results += 'Total simulation time : ' + Engine.time.to_s + ' <br><br>'+"\n"
    # @results += 'Service times of customers: ' + @service_times.to_s + ' <br><br>'+"\n"
    @results += 'Average Service time: ' + (@service_times.inject { |sum, el| sum + el }.to_f / @service_times.size).to_s + ' <br><br>'+"\n"
    # @results += 'Customer delay times: ' + @delay_times.to_s + ' <br><br>'+"\n"
    @results += 'Average delay: ' + (@delay_times.inject { |sum, el| sum + el }.to_f / @delay_times.size).to_s + ' <br><br>'+"\n"
    # @results += 'Customer time in system: ' + @time_in_system_list.to_s + ' <br><br>'+"\n"
    @results += 'Average time in system: ' + (@time_in_system_list.inject { |sum, el| sum + el }.to_f / @time_in_system_list.size).to_s + ' <br><br>'+"\n"
    calc_utilization
    entity_attrs
    puts @results
    @results
  end

  def print_user_defined_attr
    p @attr_list.to_s
  end

  attr_accessor :delay_times, :service_centers, :service_times, :interarrival_times,
                :time_in_system_list, :user_defined_attrs, :attr_list, :results

  private

  def service_avg(list)
    sum = 0
    counter = 0
    list.each do |i|
      if i != 0
        sum +=i
        counter +=1
      end
    end
    sum/counter
  end

  def calc_utilization
    @service_centers.each do |c_center|
      @results += 'Service Center: ' + c_center.name + ' <br><br>'+"\n"
      c_center.servers.to_a.each do |server|
        @results += 'server ' + server.name + ' total service time : ' + server.total_serv_time.to_s + ' <br><br>'+"\n"
        (server.total_serv_time / Engine.time * 100) > 100 ? print_utilization(server.name, 100) : print_utilization(server.name, (server.total_serv_time / Engine.time * 100))
      end
    end
  end

  def print_utilization(name, percent)
    @results += 'server ' + name + ' had ' + percent.to_s + '% utilization.' + ' <br><br>'+"\n"
  end

  def entity_attrs
    @results
  end
end