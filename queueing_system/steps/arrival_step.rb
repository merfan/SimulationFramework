require_relative '../../queueing_system/events/departure_event'
require_relative '../../event_generator'

class ArrivalStep < PipeStep
  def initialize(service_center, event)
    @service_center = service_center
    @event = event
  end

  def execute(customer)
    server = @service_center.get_free_server
    p 'A customer arrived now, id: ' + customer.__id__.to_s
    customer.add_attribute(:arrival_time, Engine.time)
    if !server.busy?
      customer.service_time = server.distribution.get_random
      p customer.service_time.to_s + ' is serv time'
      customer.add_attribute(:delay, 0)
      server.becomes_idle = Engine.time + customer.service_time
      server.seize(customer)
      # departure_event = DepartureEvent.new('departure', Engine.time + customer.service_time, customer)
      if @event == 'delegate'
        departure_event = EventGenerator.generate_event(@event, Engine.time + customer.service_time, customer, @service_center)
      else
        departure_event = EventGenerator.generate_event(@event, Engine.time + customer.service_time, customer)
      end

      Engine.fel << departure_event
    else
      @service_center.queue << customer
    end
    customer
  end

end