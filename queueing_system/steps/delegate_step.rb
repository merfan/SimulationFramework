class DelegateStep < PipeStep
  def initialize(in_service_center, out_service_center, out_event)
    @in_service_center = in_service_center
    @out_service_center = out_service_center
    @out_event = out_event
  end

  def execute(customer)
    unless @in_service_center.nil?
      server = @in_service_center.get_depart_server
      server.release(customer)

      unless @in_service_center.queue.empty?
        customer2 = @in_service_center.queue.pop
        customer2.service_time = server.distribution.get_random
        customer2.add_attribute(:delay, Engine.time - customer2.get_attribute(:arrival_time))
        server.becomes_idle = Engine.time + customer2.service_time
        server.seize(customer2)
        # departure_event = DepartureEvent.new('departure', Engine.time + customer2.service_time, customer2)
        arrival_event = EventGenerator.generate_event('delegate', Engine.time + customer2.service_time, customer2, @in_service_center)
        Engine.fel << arrival_event
      end
    end

    server_out = @out_service_center.get_free_server
    p 'A customer entered ' + server_out.name + ' whit id: ' + customer.__id__.to_s
    if !server_out.busy?
      customer.service_time = server_out.distribution.get_random
      server_out.becomes_idle = Engine.time + customer.service_time
      server_out.seize(customer)
      # departure_event = DepartureEvent.new('departure', Engine.time + customer.service_time, customer)
      if @out_event == 'delegate'
        next_event = EventGenerator.generate_event(@out_event, Engine.time + customer.service_time, customer, @out_service_center)
      else
        next_event = EventGenerator.generate_event(@out_event, Engine.time + customer.service_time, customer)
      end
      Engine.fel << next_event
    else
      @out_service_center.queue << customer
    end

    customer
  end

end