require_relative '../../event_generator'

class DepartureStep < PipeStep
  def initialize(service_center, event)
    @service_center = service_center
    @event = event
  end

  def execute(customer)
    server = @service_center.get_depart_server
    server.release(customer)
    customer.add_attribute(:time_in_system, Engine.time)
    unless @service_center.queue.empty?
      customer2 = @service_center.queue.pop
      customer2.service_time = server.distribution.get_random
      customer2.add_attribute(:delay, Engine.time - customer2.get_attribute(:arrival_time))
      server.becomes_idle = Engine.time + customer2.service_time
      server.seize(customer2)
      # departure_event = DepartureEvent.new('departure', Engine.time + customer2.service_time, customer2)
      departure_event = EventGenerator.generate_event(@event, Engine.time + customer2.service_time, customer2)
      Engine.fel << departure_event
    end
    customer
  end

end