require_relative '../../pipe_step'
require_relative '../../engine'
require_relative '../events/arrival_event'
require_relative '../../event_generator'

class GenerateArrivalEventStep < PipeStep

  def initialize(interarrival_distribution)
    @distribution = interarrival_distribution
  end

  def execute(customer)
    interarrival = @distribution.get_random
    arrival = EventGenerator.generate_event('arrival', Engine.time + interarrival, customer)
    Engine.fel << arrival
    customer
  end

end