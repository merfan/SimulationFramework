require_relative '../queueing_system/server'

class QueueingSystem
  @@service_centers = nil

  def initialize(service_centers)
    @@service_centers = service_centers
  end

  def self.service_centers
    @@service_centers
  end

  def self.service_centers=(service_centers)
    @@service_centers = service_centers
  end
end